r""" MÓDULO DE ENTIDADES

Este archivo contiene todas las entidades atomizadas de un cuadernillo de 
votación del censo de partido.

Autor: Carlos E. Rivera
Licencia: GPLv3
"""

import cv2
import pytesseract as ocr


class Votante:
    pass

class Cuadernillo:
    """ Representa la imagen de un cuadernillo de votación """

    _imagen = None # Contiene la imagen del cuadernillo.
    _votantes = list # Representa cada sector de un votante.

    def __init__(self, ruta_imagen: str):
        
        imagen = cv2.imread(ruta_imagen)[188:1503, 79:2107]

        osd_data = ocr.image_to_osd(imagen)
        angulo = float(osd_data.splitlines()[2][8:]) # Obteniendo correción de ángulo.

        if angulo > 0:
            angulo = 360 - angulo

        (h, w) = imagen.shape[:2] # (alto, ancho)
        centro = (w // 2, h // 2)
        M = cv2.getRotationMatrix2D(centro, angulo, 1.0)
        imagen_rotada = cv2.warpAffine(imagen, M, (w, h),
                flags=cv2.INTER_CUBIC, borderMode=cv2.BORDER_REPLICATE)

        # print(ocr.image_to_string(imagen_rotada))

        cv2.imwrite('resultados.jpg', imagen)


if __name__ == '__main__':
    escaner = Cuadernillo('./4691/014.JPG')
